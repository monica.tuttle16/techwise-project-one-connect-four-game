
# Includes comments to help colleagues understand the code

#========================================================================================================================================

class Connect4: # classes are always capitalized

    colors = ["red", "yellow", "blue", "green"] # static variable, pre-made list of colors
    
    def __init__(self, number_of_players = 2):
        self.grid = Grid() # initiating Grid attribute of Connect4 class
        self.players = [Player(token_color) for token_color in Connect4.colors[0:number_of_players]] # creating a list based on the static list 
        # above that is as long as the number of players selected. The default is 2, but if one enters "Connect4(4), for example, then the whole list
        # above will be created. Why is it being created again here when it's already created above? 
        # Because now, with "self" invoked, this is an attribute of the class, and will be a part of the object created when we
        # run a game loop in the next cell

    def game_loop(self): # this method (think of this like what the class does, as opposed to what it is) is the game logic for the whole operation
            for player in self.players: # here we're iterating through the list created with a list comprehension above
            # reads like: for iterator in ["red", "yellow"]: (the list to the left is the iterable, and we're assuming the default number of players of 2)

                player_selection = int(player.take_turn()) # variable created that signifies which chute within the grid(i.e. list within the list) 
                # a given player chooses, the method "take_turn" is part of the Player class (see in cell for Player class below to find this method)

                self.grid.play_token(player_selection, player.token_color) # play_token method is part of Grid class (see cell for Grid Class below)
                # the play_token method takes a player_selection argument (i.e., the chute the player chooses), and player.token_color accesses the given
                # player's color (for example, whoever takes the very first turn - this will be red, second turn - yellow, and so on)

                self.grid.draw_grid() # now that the player representing a token color has selected a chute, this method changes the grid by appending
                # to a list within the list (i.e., the chute within the grid)

                if self.grid.check_for_vertical_win(player_selection, player.token_color) or \
                self.grid.check_for_horizontal_win() or \
                self.grid.check_for_diagonal_win(player.token_color): # this method is here as a placeholder, but it doesn't
                # correctly identify diagonal wins
                    
                    print(f'{player} wins') # tells the user with a win that they won
                    return True # this ends the game loop and the program
            else:
                self.game_loop() # this continues the game loop until someone wins

chapins_game = Connect4() # we are creating an instance of the class Connect4, i.e., is an object, by making a variable to hold it
chapins_game.game_loop() # now that we have this variable that holds an instance of a class, i.e, is an object, we use the "small but mighty"
# dot operator to access the game_loop() method - it's like calling a function.

monicas_grid = Grid() # demonstrating here how we're literally making unique objects each time we call a Class in a variable
monicas_grid # see the object unique ID in the output!

aminas_grid = Grid()
aminas_grid # you may notice that the aminas_grid object unique ID is different than monicas_grid object unique ID directly above!

class Grid: # classes are always capitalized
    def __init__(self):
        self.chutes = [[],[],[],[],[],[],[]] # this is an attribute of the class, and attributes are always initiated here with the __init__ function
        # note that this is what constitutes the grid's essence/form

    def play_token(self, chute, token):
        self.chutes[chute].append(token) # in contrast to an attribute of the class, this is something the Grid class *does* (think verb for the noun)
        # So what does the play_token method do? It changes the grid initiated above by appending/adding to the end of the list
    def draw_grid(self):
        print(self.chutes) # this simply prints the grid for the user to see

    def check_for_vertical_win(self, chute, token):
        count_in_a_row = 0 # this is a counter to enforce when someone wins, once the counter equals 4
        for i in self.chutes[chute]: # so we're iterating through the [[],[],[],[],[],[],[]] to check 
        # if there's four in a row, e.g. [["red", "red", "red", "red"],[],[],[],[],[],[]]
            if i == token: # if the iterable, i.e, item within the chute/list within a list - this guy -> [] has the given player's token, 
            # for instance "red" if it's red's turn...
                count_in_a_row += 1 # then we add 1 to the count
                #print(count_in_a_row) # try uncommenting this print statement here to help you visualize the counting
                if count_in_a_row == 4: # ding ding ding, we have a winner!  The moment we have a count of 4 signifies that we have found four items
                    # stacked up on top of each other in the chute - looks like this: [["red", "red", "red", "red"],[],[],[],[],[],[]]
                    # or this [[],["yellow", "yellow", "yellow", "yellow"],[],[],[],[],[]]
                    # there are many other possibilities
                    return True # this ends the loop, returning a boolean value
            else:
                count_in_a_row = 0 # this is an interesting mechanism, this basically says that if something like this happens: 
                # [[],["yellow", "yellow", "yellow", "red",],[],[],[],[],[]] # yikes, we were *almost* to 4 with a count of 3 for yellows in sequence
                # but then red was inserted in the chute. What happens to the count then? The count is reset to 0!
        return False # returns False boolean value when there is not a count of 4 signifiying 4 tokens stacked on top of each other
    
    def check_for_horizontal_win(self): # so visually, what are we trying to find? This type of situation: 
        # [["yellow"],["yellow"],["yellow"],["yellow"],[],[],[]]] or [[["yellow", "red"],["red", "red"],["yellow", "red"],["red", "red"],[],[],[]]] ]
        # and many other possibilities - you had trouble understanding why the second example would be a win, huh? Because the index for red is 
        #the same in each list within the list - that index being "1" (think Pythonic numbering here)
        for row in range(6): 
            #print("row: ", row)
            for n in range(4):
                #print("n: ", n)
                try: # I had to inclue this for times when there is nothing in a chute, Python will yell out an error, saying there's nothing to check 
                # we want to ignore that and continue checking for "red" (or whatever turn we're on) in the same index of 4 chutes that are all next to each other 
               
                # Looking at the code below, let's remember what this code used to be when I first started working on it: 
                # def check_for_win(chutes): 
                        #if (chutes[0][0] == chutes[1][0] and chutes[1][0] == chutes[2][0] and \
                        #chutes[2][0] == chutes[3][0]) or \
                        #(chutes[1][0] == chutes[2][0] and chutes[2][0] == chutes[3][0] and \
                        #chutes[3][0] == chutes[4][0]) or \
                        #(chutes[2][0] == chutes[3][0] and chutes[3][0] == chutes[4][0] and \
                        #chutes[4][0] == chutes[5][0]) or \
                        #(chutes[3][0] == chutes[4][0] and chutes[4][0] == chutes[5][0] and \
                        #chutes[5][0] == chutes[6][0]):
                        #    return True
                        #else:
                          #  return False
                    # That's a lot of code, right? AND it only allows us to check the bottom row ([0]) in the grid - imagine how much more
                    # "hard coding" we'd have to do to capture every row being checked for a horizontal win.
                    # Since we need to be able to check ALL rows for a possible horizontal win,
                    # with the improved logic below, we now can check the chute's index ("n") for all rows of a Connect 4 grid
                    if (self.chutes[n][row] == self.chutes[n + 1][row] and self.chutes[n + 1][row] == self.chutes[n + 2][row] and \
                    self.chutes[n + 2][row] == self.chutes[n + 3][row]):
                        return True # 
                except IndexError: # again, if there's an error thrown because we have stumbled upon an empty chute [], we tell Python to ignore it
                    pass # ignoring the error and...
                continue # continuing the loop
        return False 

    # I don't think the code below works, was insprid by this code from
    # https://stackoverflow.com/questions/29949169/python-connect-4-check-win-function and I modified it. 
    # It looked sensible to me because it has a similar logical approach as the horizontal win, but we need to change this,
    # because I tested it with some use cases, and while it runs, it doesn't correctly identify diagonal wins
    def check_for_diagonal_win(self, token_color):
        for row in range(4):
            for n in range(3):
                try:
                    if self.chutes[row][n] == token_color and self.chutes[row][n + 1] == token_color and self.chutes[row][n + 2] == token_color and self.chutes[row][n + 3] == token_color:
                        return True
                except IndexError:
                    next
        return False

class Player:
    def __init__(self, token_color):
        self.token_color = token_color # essential attribute of a player is its token color
    
    def __str__(self):
        return str(self.token_color)

    def __repr__(self):
        return f'{self.token_color}'

    def take_turn(self):
        return input("Select a chute: ") # this method is accessed above in the Connect4 game loop by asking
        # asking the user to select a chute within the grid (i.e., a list within the list)

#------------------------------------------------------------------------------------------------------------------------------------------
# Research for a potential AI class
"""To make the best decision, the AI needs to do the following:

    Store the current state (values) of the tic-tac-toe board in an array. (For any empty cell, the cell’s index will get stored as its present content).
    Get an array list of only the empty cells’ indexes.
    Check and confirm if a specific player has won the game.
    Recursively invoke minimax on each of the board’s empty cells.
    Return a score for every possible move for both player X and player O.
    Out of all the returned scores, choose the best one (the highest) that is guaranteed to minimize the human player’s possibilities of winning the game.
"""

class AI:
    def __init__(self, token_color, difficulty_level):
        self.token_color = token_color
        self.difficulty_level = difficulty_level
       # invoke a grid object somehow self.current_grid_state = current_grid_state

