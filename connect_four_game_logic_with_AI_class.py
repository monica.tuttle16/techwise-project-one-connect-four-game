# -*- coding: utf-8 -*-
"""

Updated 8/19/22

Author: Monica Tuttle, with contributions from Chapin Meny

"""

import numpy as np
import time 
import random

class Player: 
    def __init__(self, token_color):
        self.token_color = token_color
    
    def __repr__(self):
        return self.token_color

    def take_turn(self):
        user_input = input(f"Player {self.token_color}, select a chute: ") 
        return user_input

class AI (Player): 
    def __init__(self, token_color):
        super().__init__(token_color) 
    
    def take_turn(self):
        AI_selection = random.randrange(1, 8) 
        print(f'Player {self.token_color} (an AI) will now select chute {AI_selection}.')
        time.sleep(1)
        return AI_selection
        
class Grid: 
    def __init__(self):
        self.board = np.zeros((6, 7)) 

    def play_token(self, col, token): 
        for r in range(6):
            if self.board[r][col] == 0:
                row = r
                break
        self.board[row][col] = token
            
    def is_a_location(self, col):
        return self.board[6 - 1][col] == 0

    def draw_grid(self):
        print(np.flip(self.board, 0)) 

    def check_for_win(self, board, token):
        for c in range(7):
            for r in range(6 - 3):
                if self.board[r][c] == token and self.board[r + 1][c] == token and self.board[r + 2][c] == token and self.board[r + 3][c] == token:
                    return True

        # checking for horizontal win
        for c in range(7 - 3): 
            for r in range(6):
                if self.board[r][c] == token and self.board[r][c + 1] == token and self.board[r][c + 2] == token and self.board[r][c + 3] == token:
                    return True

        # checking for positively sloped diagonals
        for c in range(7 - 3):
            for r in range(6 - 3):
                if self.board[r][c] == token and self.board[r + 1][c + 1] == token and self.board[r + 2][c + 2] == token and self.board[r + 3][c + 3] == token:
                    return True

        # checking for negatively sloped diagonals
        for c in range(7 - 3):
            for r in range(3, 6):
                if self.board[r][c] == token and self.board[r - 1][c + 1] == token and self.board[r - 2][c + 2] == token and self.board[r - 3][c + 3] == token:
                    return True
                    
import numpy as np
import time 

class Connect4: 

    colors = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] 

    def __init__(self):
        self.grid = Grid() 
        self.num_players = int(input("How many players? "))
        self.players = []
        self.moves_played = []

        for player in range(self.num_players):
            is_ai = input(f'Is player {player + 1} an AI? (y/n)') 
            if is_ai == 'y':
                self.players.append(AI(Connect4.colors[player]))
            elif is_ai == 'n':
                self.players.append(Player(Connect4.colors[player]))
        self.game_loop()    

    def game_loop(self): 
        while len(self.moves_played) < (6 * 7):
            for player in self.players:
                player_selection = int(player.take_turn()) - 1
                while player_selection not in list(range(0, 7)):
                    print("Invalid entry")
                    print()
                    player_selection = int(player.take_turn()) - 1
                while self.grid.is_a_location(player_selection) == False:
                    print()
                    print("That chute is full. Another chute needs to be selected.")
                    print()
                    player_selection = int(player.take_turn()) - 1
                else:
                    self.grid.play_token(player_selection, player.token_color) 
                    self.moves_played.append(player_selection)
                    print()
                    self.grid.draw_grid()
                    print()
                    if len(self.moves_played) == (6 * 7):
                        print("All chutes are full with no win. Game over.")
                        return False
                    if self.grid.check_for_win(player_selection, player.token_color):
                        print(f'Player {player.token_color} wins.') 
                        return True
