## Name
Connect Four

## Description
This project delivers a Connect Four game built within an Object-Oriented Programming framework that includes multi-player functionality and the option to play against a computer. 

Contributors:

Monica Tuttle and Chapin Meny

## Project status

In Progress. 

## Roadmap

1. Optimize AI class (include a more sophisticated algorithm) 
2. Optimize reusablity of code, flexibility in changing code, and flexibility in game play.
3. Create version with GUI using the pygame library.

## Output/Visuals

![cropped-visual-connect-four.png](./cropped-visual-connect-four.png)

## Acknowledgments

Nick Groesch

